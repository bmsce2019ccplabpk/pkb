
#include<stdio.h>
int main( )
{
    FILE *fp; char ch;
    fp=fopen("file.txt", "w");
    printf("Enter the data to be stored\n");
    printf("Press ctrl+d for stopping the process of entering the data ");
    while((ch=getchar())!=EOF)
    {
        putc(ch, fp);
    }
    fclose(fp);
    fp=fopen("file.txt", "r");
    printf("Printing from file...\n");
    while((ch=getc(fp))!=EOF)
    {
        printf("%c", ch);
    }
    printf("\n");
    fclose(fp); 
    return 0;
}

